(function ($) {
  'use strict';
  var pluginName = 'pertimm';

  $[pluginName] = (function () {
    /**
     * Plugin Constructor. This function build the basic object for the plugin
     * @param (object) element - The jQuery or Zepto DOM element
     * @param (object) options - A list of options for the plugin
     */

     $[pluginName] = function (element, options) {
      // Plugin params...
      this.options                = $.extend({}, options);
      this.$oSearchField          = $(element);                   // @param (object) element - The jQuery or Zepto DOM element
      this.$sUrl                  = this.options.ajaxUrl;   // Ajax Url found in plugin data-component param.
      this.$minChars              = this.options.minChars || 1 ; // Minimum number of chars before the autocompletion is launched
      // Common Css classes
      this.$autocompletionClass   = 'js-query-autocompleter';
      this.$jAutocompleter        = 'js-query_first_field-autocompleter-indicator';
      // init plugin
      return this._Pertimm();
    };
    $[pluginName].prototype = {

      _Pertimm: function () {
        var that = this, xhr;
        if (this.$sUrl) {
          // Dinamically adding the new DIV tag close to the search input parent node
          $('<div/>', {id: this.$jAutocompleter}).addClass(this.$autocompletionClass).insertAfter(this.$oSearchField.parent());

          this.$oSearchField.keypress(function (event) {
            // Firefox & IE8 fix with keyCode instead of witch event
             if((event.keyCode === 38) || (event.keyCode === 40)){
                 return false;
              }else{
                var get_ww_search_word,
                sKeyword = $(this).val();
                if (sKeyword.length >= that.$minChars) {
                  // Execute all request but only processes the result of the last keyup submit
                  if(xhr && xhr.readystate != 4){
                    xhr.abort();
                  }
                  xhr = $.ajax({
                    type: "POST",
                    url: that.$sUrl,
                    data : {search_word: sKeyword},
                    success: function(data){
                      /* optional stuff to do after success */
                      if ($(data).find('li').length > 0) {
                        $(that.$oSearchField).parent().next().html(data).show().find('li').click(function () {
                          that.$oSearchField.val($(this).text().replace('.', ' ')).focus().closest('form').submit();
                        });
                      }
                    }
                  });
                // end post
                }
            }
        });
          // Navigation list with arrow keys
          var currentSelection = 0;
          this.$oSearchField.keydown(function(event){
            if(event.keyCode === 40){
              navigate('down');
            }else if(event.keyCode === 38) {
              navigate('up');
            }
          });
          var navigate = function(direction){
            var resultListSize  = $("."+that.$autocompletionClass).find('ul li').size(); // 5
            if(direction == 'up') {
              (currentSelection == resultListSize || currentSelection == 1 ) ? currentSelection = 1 : currentSelection--;;
            }
            else if (direction == 'down') {
              (currentSelection == resultListSize ) ? currentSelection = 1 : currentSelection++;;
            }
              setSelected(currentSelection);
          };
          var setSelected = function(menuitem){

              var resultList = $("."+that.$autocompletionClass).find('ul li'),
                  removeliSelected = resultList.removeClass("liSelected"),
                  liSelected = resultList.eq(menuitem-1).addClass("liSelected"),
                  liSelectedValue = liSelected.text();
                  that.$oSearchField.val(liSelectedValue);
          };
        }
        // Close result list on click to any part of the DOM
        $(document).click(function (e){
          if (!$(that.$oSearchField).is(e.target)){
            $(that.$oSearchField).parent().next().hide();
          }
        });
      }
    };
    // Building the plugin
    /**
     * The plugin component
     * @param  {object} options - list of all parameters for the jQuery/Zepto module
     * @return {object} - The jQuery/Zepto DOM element
     */
     return $[pluginName];
   }(window));

$.fn[pluginName] = function (options) {
  return this.each(function () {
    if (!$(this).data(pluginName)) {

      if (options === 'destroy') {
        return;
      }
      $(this).data(pluginName, new $[pluginName](this, options));
    }
    else {
      var $plugin = $(this).data(pluginName);
    }
  });
};
})(window.Zepto || window.jQuery);
